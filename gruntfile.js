module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
        //Read the package.json (optional)
        pkg: grunt.file.readJSON('package.json'),
        // Metadata.
        meta: {
            basePath: '/',
            srcPath: 'scss/',
            deployPath: 'css/'
        },
        // Task configuration.
        sass: {
            dist: {
                files: {
                    '<%= meta.deployPath %>all.css': '<%= meta.srcPath %>all.scss',
                    '<%= meta.deployPath %>ie.css': '<%= meta.srcPath %>ie.scss',
					'<%= meta.deployPath %>ie9.css': '<%= meta.srcPath %>ie9.scss',
					'<%= meta.deployPath %>ie8.css': '<%= meta.srcPath %>ie8.scss',
                    '<%= meta.deployPath %>large.css': '<%= meta.srcPath %>large.scss',
                    '<%= meta.deployPath %>med.css': '<%= meta.srcPath %>med.scss',
                    '<%= meta.deployPath %>print.css': '<%= meta.srcPath %>print.scss',
                    '<%= meta.deployPath %>small.css': '<%= meta.srcPath %>small.scss',
                    '<%= meta.deployPath %>xsmall.css': '<%= meta.srcPath %>xsmall.scss',
                }
            }
        },
        watch: {
            scripts: {
                files: [
                    '<%= meta.srcPath %>scss/*.scss',
                    '<%= meta.srcPath %>scss/partials/*.scss',
                    '<%= meta.srcPath %>scss/partials/**/*.scss',
                    '<%= meta.srcPath %>scss/partials/sei-site/**/*.scss',
                    '<%= meta.srcPath %>scss/partials/sei-site/**/**/*.scss',
					'<%= meta.srcPath %>scss/partials/bootstrap/*.scss',
                    '<%= meta.srcPath %>scss/partials/bootstrap/**/*.scss',
                ],
                tasks: ['sass']
            }
        },
    });
    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    // Default task
    grunt.registerTask('default', ['sass']);
    grunt.registerTask('dist', ['watch']);
};